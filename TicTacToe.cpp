#include <iostream>
#include <set>
#include <string>

void displayBoard(const char *boardState)
{
  std::cout << " " << std::endl;
  std::cout << " " << boardState[0] << " | " << boardState[1] << " | " << boardState[2] << std::endl;
  std::cout << " "
            << "---------" << std::endl;
  std::cout << " " << boardState[3] << " | " << boardState[4] << " | " << boardState[5] << std::endl;
  std::cout << " "
            << "---------" << std::endl;
  std::cout << " " << boardState[6] << " | " << boardState[7] << " | " << boardState[8] << std::endl;
  std::cout << " " << std::endl;
}

std::string getValidMoves(const std::set<std::string> moves)
{
  std::string str = "";
  std::set<std::string>::iterator it;
  for (it = moves.begin(); it != moves.end(); ++it)
  {
    str += " " + *it;
  }
  return str;
}

int main()
{
  char boardState[] = "123456789";
  std::set<std::string> validMoves = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};

  char player = 'x';
  bool playing = true;

  while (playing)
  {
    displayBoard(boardState);

    // Read player move
    std::string move;
    bool validMove = false;
    std::cout << "Player " << player << " make a move: ";
    std::getline(std::cin, move);

    // Validate move
    std::set<std::string>::iterator it = validMoves.find(move);
    if (move.size() != 1 || it == validMoves.end())
    {
      std::cout << move << " is not a valid move. Valid moves are" << getValidMoves(validMoves) << "." << std::endl;
      continue;
    }

    // Update game state
    boardState[std::stoi(move) - 1] = player;
    validMoves.erase(move);

    // Check if game has been won
    bool gameOver = false;
    if (boardState[0] == boardState[1] && boardState[0] == boardState[2])
      gameOver = true;
    if (boardState[3] == boardState[4] && boardState[3] == boardState[5])
      gameOver = true;
    if (boardState[6] == boardState[7] && boardState[6] == boardState[8])
      gameOver = true;
    if (boardState[0] == boardState[3] && boardState[0] == boardState[6])
      gameOver = true;
    if (boardState[1] == boardState[4] && boardState[1] == boardState[7])
      gameOver = true;
    if (boardState[2] == boardState[5] && boardState[2] == boardState[8])
      gameOver = true;
    if (boardState[0] == boardState[4] && boardState[0] == boardState[8])
      gameOver = true;
    if (boardState[2] == boardState[4] && boardState[2] == boardState[6])
      gameOver = true;

    if (gameOver)
    {
      displayBoard(boardState);
      std::string again;
      std::cout << "Player " << player << " has won! Would you like to play again (y/n)? ";
      std::getline(std::cin, again);

      if (again == "y")
      {
        main();
      }
      return 0;
    }

    // Switch players
    if (player == 'x')
    {
      player = 'o';
    }
    else
    {
      player = 'x';
    }
  }
}